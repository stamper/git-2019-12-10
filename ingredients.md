# Ingredients for delicious guacamole

* 2 avocados
* 1 lime
* 1/2 onion
* 2 tsp salt
* [1/2 lb fresh sweet peas](https://cooking.nytimes.com/recipes/1015047-green-pea-guacamole)
